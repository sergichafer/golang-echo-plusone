package handler

import (
	"os"
	"testing"

	"encoding/json"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/labstack/echo"
	"gitlab.com/sergislm4/golang-echo-plusone/db"
	"gitlab.com/sergislm4/golang-echo-plusone/model"
	"gitlab.com/sergislm4/golang-echo-plusone/plan"
	"gitlab.com/sergislm4/golang-echo-plusone/router"
	"gitlab.com/sergislm4/golang-echo-plusone/store"
	"gitlab.com/sergislm4/golang-echo-plusone/user"
)

var (
	d  *gorm.DB
	us user.Store
	as plan.Store
	h  *Handler
	e  *echo.Echo
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	tearDown()
	os.Exit(code)
}

func authHeader(token string) string {
	return "Token " + token
}

func setup() {
	d = db.TestDB()
	db.AutoMigrate(d)
	us = store.NewUserStore(d)
	as = store.NewPlanStore(d)
	h = NewHandler(us, as)
	e = router.New()
	loadFixtures()
}

func tearDown() {
	d = db.TestDB()
	db.DropTestDB(d)
}

func responseMap(b []byte, key string) map[string]interface{} {
	var m map[string]interface{}
	json.Unmarshal(b, &m)
	return m[key].(map[string]interface{})
}

func loadFixtures() error {
	u1bio := "user1 bio"
	u1image := "http://realworld.io/user1.jpg"
	u1 := model.User{
		Username: "user1",
		Email:    "user1@realworld.io",
		Bio:      &u1bio,
		Image:    &u1image,
	}
	u1.Password, _ = u1.HashPassword("secret")
	if err := us.Create(&u1); err != nil {
		return err
	}

	u2bio := "user2 bio"
	u2image := "http://realworld.io/user2.jpg"
	u2 := model.User{
		Username: "user2",
		Email:    "user2@realworld.io",
		Bio:      &u2bio,
		Image:    &u2image,
	}
	u2.Password, _ = u2.HashPassword("secret")
	if err := us.Create(&u2); err != nil {
		return err
	}

	a := model.Plan{
		Slug:        "plan1-slug",
		Title:       "plan1 title",
		Description: "plan1 description",
		AuthorID:    1,
		Tags: []model.Tag{
			{
				Tag: "tag1",
			},
			{
				Tag: "tag2",
			},
		},
	}
	as.CreatePlan(&a)

	a2 := model.Plan{
		Slug:        "plan2-slug",
		Title:       "plan2 title",
		Description: "plan2 description",
		AuthorID:    2,
		Tags: []model.Tag{
			{
				Tag: "tag1",
			},
		},
	}
	as.CreatePlan(&a2)

	return nil
}
