package handler

import (
	"gitlab.com/sergislm4/golang-echo-plusone/plan"
	"gitlab.com/sergislm4/golang-echo-plusone/user"
)

// Struct to define a handler object to work with both stores
type Handler struct {
	userStore user.Store
	planStore plan.Store
}

// Function to open an instance of the handler
func NewHandler(us user.Store, as plan.Store) *Handler {
	return &Handler{
		userStore: us,
		planStore: as,
	}
}
