package handler

import (
	"github.com/labstack/echo"
	"gitlab.com/sergislm4/golang-echo-plusone/router/middleware"
	"gitlab.com/sergislm4/golang-echo-plusone/utils"
)

func (h *Handler) Register(v1 *echo.Group) {
	jwtMiddleware := middleware.JWT(utils.JWTSecret)
	guestUsers := v1.Group("/users")
	guestUsers.POST("", h.SignUp)
	guestUsers.POST("/login", h.Login)

	user := v1.Group("/user", jwtMiddleware)
	user.GET("", h.CurrentUser)
	user.PUT("", h.UpdateUser)

	profiles := v1.Group("/profiles", jwtMiddleware)
	profiles.GET("/:username", h.GetProfile)

	plans := v1.Group("/plans", middleware.JWTWithConfig(
		middleware.JWTConfig{
			Skipper: func(c echo.Context) bool {
				if c.Request().Method == "GET" && c.Path() != "/api/plans/feed" {
					return true
				}
				return false
			},
			SigningKey: utils.JWTSecret,
		},
	))
	plans.POST("", h.CreatePlan)
	plans.GET("/feed", h.Feed)
	plans.PUT("/:slug", h.UpdatePlan)
	plans.PUT("/:slug/join", h.JoinPlan)
	plans.DELETE("/:slug", h.DeletePlan)
	plans.GET("", h.Plans)
	plans.GET("/:slug", h.GetPlan)

	tags := v1.Group("/tags")
	tags.GET("", h.Tags)

	sports := v1.Group("/sports")
	sports.GET("", h.Sports)

	contact := v1.Group("/contact")
	contact.POST("", h.SendMail)
}
