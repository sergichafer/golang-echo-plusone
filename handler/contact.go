package handler

import (
	"net/http"
	"os"

	"github.com/labstack/echo"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"gitlab.com/sergislm4/golang-echo-plusone/utils"
)

// Function create a new email structure
func contactEmail(c *contactRequest) []byte {
	subject := "New contact mail from " + c.ContactData.Email + " via PlusOne"
	address := c.ContactData.Email
	name := c.ContactData.Email
	from := mail.NewEmail(name, address)
	address = "sergislm4@gmail.com"
	name = "PlusOne"
	to := mail.NewEmail(name, address)
	content := mail.NewContent("text/plain", c.ContactData.Body)
	m := mail.NewV3MailInit(from, subject, to, content)
	return mail.GetRequestBody(m)
}

// Function to send an email structure using sendrid api key
func (h *Handler) SendMail(c echo.Context) error {
	req := &contactRequest{}
	if err := req.bind(c); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, utils.NewError(err))
	}
	request := sendgrid.GetRequest(os.Getenv("SENDGRID_API_KEY"), "/v3/mail/send", "https://api.sendgrid.com")
	request.Method = "POST"
	var Body = contactEmail(req)
	request.Body = Body
	response, err := sendgrid.API(request)

	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, utils.NewError(err))
	} else {
		return c.JSON(response.StatusCode, req)
	}
}
