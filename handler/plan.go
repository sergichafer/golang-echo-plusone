package handler

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/sergislm4/golang-echo-plusone/model"
	"gitlab.com/sergislm4/golang-echo-plusone/utils"
)

// Function used to retrieve one single plan based on a slug
func (h *Handler) GetPlan(c echo.Context) error {
	slug := c.Param("slug")
	a, err := h.planStore.GetBySlug(slug)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, utils.NewError(err))
	}
	if a == nil {
		return c.JSON(http.StatusNotFound, utils.NotFound())
	}
	return c.JSON(http.StatusOK, newPlanResponse(c, a))
}

// Function used to return the plans based on a few params that act as conditions for the api
func (h *Handler) Plans(c echo.Context) error {
	sport := c.QueryParam("sport")
	tag := c.QueryParam("tag")
	author := c.QueryParam("author")
	offset, err := strconv.Atoi(c.QueryParam("offset"))
	if err != nil {
		offset = 0
	}
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		limit = 20
	}
	var plans []model.Plan
	var count int
	if tag != "" {
		plans, count, err = h.planStore.ListByTag(tag, offset, limit)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, nil)
		}
	} else if sport != "" {
		plans, count, err = h.planStore.ListBySport(sport, offset, limit)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, nil)
		}
	} else if author != "" {
		plans, count, err = h.planStore.ListByAuthor(author, offset, limit)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, nil)
		}
	} else {
		plans, count, err = h.planStore.List(offset, limit)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, nil)
		}
	}
	return c.JSON(http.StatusOK, newPlanListResponse(h.userStore, userIDFromToken(c), plans, count))
}

// Function used to return the plans that the used has joined
func (h *Handler) Feed(c echo.Context) error {
	var plans []model.Plan
	var count int
	offset, err := strconv.Atoi(c.QueryParam("offset"))
	if err != nil {
		offset = 0
	}
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		limit = 20
	}
	plans, count, err = h.planStore.ListFeed(userIDFromToken(c), offset, limit)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, nil)
	}
	return c.JSON(http.StatusOK, newPlanListResponse(h.userStore, userIDFromToken(c), plans, count))
}

// Function used to create a new plan in the database
func (h *Handler) CreatePlan(c echo.Context) error {
	var a model.Plan
	req := &planCreateRequest{}
	if err := req.bind(c, &a); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, utils.NewError(err))
	}
	a.AuthorID = userIDFromToken(c)
	err := h.planStore.CreatePlan(&a)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, utils.NewError(err))
	}

	return c.JSON(http.StatusCreated, newPlanResponse(c, &a))
}

// Function used to update an existing plan
func (h *Handler) UpdatePlan(c echo.Context) error {
	slug := c.Param("slug")
	a, err := h.planStore.GetUserPlanBySlug(userIDFromToken(c), slug)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, utils.NewError(err))
	}
	if a == nil {
		return c.JSON(http.StatusNotFound, utils.NotFound())
	}
	req := &planUpdateRequest{}
	req.populate(a)
	if err := req.bind(c, a); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, utils.NewError(err))
	}
	if err = h.planStore.UpdatePlan(a, req.Plan.Tags); err != nil {
		return c.JSON(http.StatusInternalServerError, utils.NewError(err))
	}
	return c.JSON(http.StatusOK, newPlanResponse(c, a))
}

// Function used to join an existing plan
func (h *Handler) JoinPlan(c echo.Context) error {
	slug := c.Param("slug")
	a, err := h.planStore.GetBySlug(slug)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, utils.NewError(err))
	}
	if a == nil {
		return c.JSON(http.StatusNotFound, utils.NotFound())
	}
	req := &planUpdateRequest{}
	req.populate(a)
	if err = h.planStore.JoinPlan(a, userIDFromToken(c)); err != nil {
		return c.JSON(http.StatusInternalServerError, utils.NewError(err))
	}
	return c.JSON(http.StatusOK, newPlanResponse(c, a))
}

// Function used to delete and existing plan
func (h *Handler) DeletePlan(c echo.Context) error {
	slug := c.Param("slug")
	a, err := h.planStore.GetUserPlanBySlug(userIDFromToken(c), slug)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, utils.NewError(err))
	}
	if a == nil {
		return c.JSON(http.StatusNotFound, utils.NotFound())
	}
	err = h.planStore.DeletePlan(a)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, utils.NewError(err))
	}
	return c.JSON(http.StatusOK, map[string]interface{}{"result": "ok"})
}

// Function used to return all the tags created
func (h *Handler) Tags(c echo.Context) error {
	tags, err := h.planStore.ListTags()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, newTagListResponse(tags))
}

// Function used to return all available sports
func (h *Handler) Sports(c echo.Context) error {
	sports, err := h.planStore.ListSports()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, newSportListResponse(sports))
}
