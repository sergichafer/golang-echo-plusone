package handler

import (
	"time"

	"github.com/labstack/echo"
	"gitlab.com/sergislm4/golang-echo-plusone/model"
	"gitlab.com/sergislm4/golang-echo-plusone/user"
	"gitlab.com/sergislm4/golang-echo-plusone/utils"
)

type userResponse struct {
	User struct {
		Username string  `json:"username"`
		Email    string  `json:"email"`
		Bio      *string `json:"bio"`
		Image    *string `json:"image"`
		Token    string  `json:"token"`
	} `json:"user"`
}

func newUserResponse(u *model.User) *userResponse {
	r := new(userResponse)
	r.User.Username = u.Username
	r.User.Email = u.Email
	r.User.Bio = u.Bio
	r.User.Image = u.Image
	r.User.Token = utils.GenerateJWT(u.ID)
	return r
}

type profileResponse struct {
	Profile struct {
		Username string  `json:"username"`
		Bio      *string `json:"bio"`
		Image    *string `json:"image"`
	} `json:"profile"`
}

func newProfileResponse(us user.Store, userID uint, u *model.User) *profileResponse {
	r := new(profileResponse)
	r.Profile.Username = u.Username
	r.Profile.Bio = u.Bio
	r.Profile.Image = u.Image
	return r
}

type planResponse struct {
	Slug        string    `json:"slug"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Width       int       `json:"width"`
	Height      int       `json:"height"`
	Image       string    `json:"image"`
	Start_date  time.Time `json:"start_date"`
	End_date    time.Time `json:"end_date"`
	Active      int       `json:"active"`
	Location    string    `json:"location"`
	Sport       struct {
		Refer uint   `json:"refer"`
		Sport string `json:"sport"`
	} `json:"sport"`
	TagList   []string  `json:"tagList"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	Author    struct {
		Username string  `json:"username"`
		Bio      *string `json:"bio"`
		Image    *string `json:"image"`
	} `json:"author"`
	Participants []*profileResponse `json:"participants"`
}

type sportResponse struct {
	Refer uint   `json:"refer"`
	Sport string `json:"sport"`
}

type singleSportResponse struct {
	Plan *planResponse `json:"plan"`
}

type singlePlanResponse struct {
	Plan *planResponse `json:"plan"`
}

type planListResponse struct {
	Plans      []*planResponse `json:"plans"`
	PlansCount int             `json:"plansCount"`
}

func newPlanResponse(c echo.Context, a *model.Plan) *singlePlanResponse {
	ar := new(planResponse)
	ar.TagList = make([]string, 0)
	ar.Slug = a.Slug
	ar.Title = a.Title
	ar.Description = a.Description
	ar.Width = a.Width
	ar.Height = a.Height
	ar.Image = a.Image
	ar.Start_date = a.Start_date
	ar.End_date = a.End_date
	ar.Active = a.Active
	ar.Location = a.Location
	ar.CreatedAt = a.CreatedAt
	ar.UpdatedAt = a.UpdatedAt
	ar.Sport.Sport = a.Sport.Sport
	for _, t := range a.Tags {
		ar.TagList = append(ar.TagList, t.Tag)
	}
	ar.Author.Username = a.Author.Username
	ar.Author.Image = a.Author.Image
	ar.Author.Bio = a.Author.Bio
	ar.Participants = make([]*profileResponse, 0)
	for _, u := range a.Participants {
		pr := new(profileResponse)
		pr.Profile.Username = u.Username
		pr.Profile.Bio = u.Bio
		pr.Profile.Image = u.Image
		ar.Participants = append(ar.Participants, pr)
	}
	return &singlePlanResponse{ar}
}

func newPlanListResponse(us user.Store, userID uint, plans []model.Plan, count int) *planListResponse {
	r := new(planListResponse)
	r.Plans = make([]*planResponse, 0)
	for _, a := range plans {
		ar := new(planResponse)
		ar.TagList = make([]string, 0)
		ar.Slug = a.Slug
		ar.Title = a.Title
		ar.Description = a.Description
		ar.Width = a.Width
		ar.Height = a.Height
		ar.Image = a.Image
		ar.Start_date = a.Start_date
		ar.End_date = a.End_date
		ar.Active = a.Active
		ar.Location = a.Location
		ar.CreatedAt = a.CreatedAt
		ar.UpdatedAt = a.UpdatedAt
		ar.Sport.Sport = a.Sport.Sport
		for _, t := range a.Tags {
			ar.TagList = append(ar.TagList, t.Tag)
		}
		ar.Author.Username = a.Author.Username
		ar.Author.Image = a.Author.Image
		ar.Author.Bio = a.Author.Bio
		ar.Participants = make([]*profileResponse, 0)
		for _, u := range a.Participants {
			pr := new(profileResponse)
			pr.Profile.Username = u.Username
			pr.Profile.Bio = u.Bio
			pr.Profile.Image = u.Image
			ar.Participants = append(ar.Participants, pr)
		}
		r.Plans = append(r.Plans, ar)
	}
	r.PlansCount = count
	return r
}

type sportListResponse struct {
	Sports []*sportResponse `json:"sports"`
}

func newSportListResponse(sports []model.Sport) *sportListResponse {
	r := new(sportListResponse)
	r.Sports = make([]*sportResponse, 0)
	for _, a := range sports {
		ar := new(sportResponse)
		ar.Refer = a.Refer
		ar.Sport = a.Sport
		r.Sports = append(r.Sports, ar)
	}
	return r
}

type tagListResponse struct {
	Tags []string `json:"tags"`
}

func newTagListResponse(tags []model.Tag) *tagListResponse {
	r := new(tagListResponse)
	for _, t := range tags {
		r.Tags = append(r.Tags, t.Tag)
	}
	return r
}
