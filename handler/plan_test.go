package handler

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sergislm4/golang-echo-plusone/router"
	"gitlab.com/sergislm4/golang-echo-plusone/router/middleware"
	"gitlab.com/sergislm4/golang-echo-plusone/utils"
)

func TestListPlansCaseSuccess(t *testing.T) {
	tearDown()
	setup()
	e := router.New()
	req := httptest.NewRequest(echo.GET, "/api/plans", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	assert.NoError(t, h.Plans(c))
	if assert.Equal(t, http.StatusOK, rec.Code) {
		var aa planListResponse
		err := json.Unmarshal(rec.Body.Bytes(), &aa)
		assert.NoError(t, err)
		assert.Equal(t, 2, aa.PlansCount)
	}
}

func TestGetPlansCaseSuccess(t *testing.T) {
	tearDown()
	setup()
	req := httptest.NewRequest(echo.GET, "/api/plans/:slug", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/api/plans/:slug")
	c.SetParamNames("slug")
	c.SetParamValues("plan1-slug")
	assert.NoError(t, h.GetPlan(c))
	if assert.Equal(t, http.StatusOK, rec.Code) {
		var a singlePlanResponse
		err := json.Unmarshal(rec.Body.Bytes(), &a)
		assert.NoError(t, err)
		assert.Equal(t, "plan1-slug", a.Plan.Slug)
		assert.Equal(t, 2, len(a.Plan.TagList))
	}
}

func TestCreatePlansCaseSuccess(t *testing.T) {
	tearDown()
	setup()
	var (
		reqJSON = `{"plan":{"title":"plan2", "description":"plan2", "tagList":["tag1","tag2"]}}`
	)
	jwtMiddleware := middleware.JWT(utils.JWTSecret)
	req := httptest.NewRequest(echo.POST, "/api/plans", strings.NewReader(reqJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	req.Header.Set(echo.HeaderAuthorization, authHeader(utils.GenerateJWT(1)))
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	err := jwtMiddleware(func(context echo.Context) error {
		return h.CreatePlan(c)
	})(c)
	assert.NoError(t, err)
	if assert.Equal(t, http.StatusCreated, rec.Code) {
		var a singlePlanResponse
		err := json.Unmarshal(rec.Body.Bytes(), &a)
		assert.NoError(t, err)
		assert.Equal(t, "plan2", a.Plan.Slug)
		assert.Equal(t, "plan2", a.Plan.Description)
		assert.Equal(t, "plan2", a.Plan.Title)
		assert.Equal(t, 2, len(a.Plan.TagList))
	}
}

func TestUpdatePlansCaseSuccess(t *testing.T) {
	tearDown()
	setup()
	var (
		reqJSON = `{"plan":{"title":"plan1 part 2", "tagList":["tag3"]}}`
	)
	jwtMiddleware := middleware.JWT(utils.JWTSecret)
	req := httptest.NewRequest(echo.PUT, "/api/plans/:slug", strings.NewReader(reqJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	req.Header.Set(echo.HeaderAuthorization, authHeader(utils.GenerateJWT(1)))
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/api/plans/:slug")
	c.SetParamNames("slug")
	c.SetParamValues("plan1-slug")
	err := jwtMiddleware(func(context echo.Context) error {
		return h.UpdatePlan(c)
	})(c)
	assert.NoError(t, err)
	if assert.Equal(t, http.StatusOK, rec.Code) {
		var a singlePlanResponse
		err := json.Unmarshal(rec.Body.Bytes(), &a)
		assert.NoError(t, err)
		assert.Equal(t, "plan1 part 2", a.Plan.Title)
		assert.Equal(t, "plan1-part-2", a.Plan.Slug)
		assert.Equal(t, 1, len(a.Plan.TagList))
		assert.Equal(t, "tag3", a.Plan.TagList[0])
	}
}

func TestDeletePlanCaseSuccess(t *testing.T) {
	tearDown()
	setup()
	jwtMiddleware := middleware.JWT(utils.JWTSecret)
	req := httptest.NewRequest(echo.DELETE, "/api/plans/:slug", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	req.Header.Set(echo.HeaderAuthorization, authHeader(utils.GenerateJWT(1)))
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/api/plans/:slug")
	c.SetParamNames("slug")
	c.SetParamValues("plan1-slug")
	err := jwtMiddleware(func(context echo.Context) error {
		return h.DeletePlan(c)
	})(c)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, rec.Code)
}

func TestGetTagsCaseSuccess(t *testing.T) {
	tearDown()
	setup()
	req := httptest.NewRequest(echo.GET, "/api/tags", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	assert.NoError(t, h.Tags(c))
	if assert.Equal(t, http.StatusOK, rec.Code) {
		var tt tagListResponse
		err := json.Unmarshal(rec.Body.Bytes(), &tt)
		assert.NoError(t, err)
		assert.Equal(t, 2, len(tt.Tags))
		assert.Contains(t, tt.Tags, "tag1")
		assert.Contains(t, tt.Tags, "tag2")
	}
}
