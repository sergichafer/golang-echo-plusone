package handler

import (
	"time"

	"github.com/gosimple/slug"
	"github.com/labstack/echo"
	"gitlab.com/sergislm4/golang-echo-plusone/model"
)

type userUpdateRequest struct {
	User struct {
		Username string `json:"username"`
		Email    string `json:"email" validate:"email"`
		Password string `json:"password"`
		Bio      string `json:"bio"`
		Image    string `json:"image"`
	} `json:"user"`
}

func newUserUpdateRequest() *userUpdateRequest {
	return new(userUpdateRequest)
}

func (r *userUpdateRequest) populate(u *model.User) {
	r.User.Username = u.Username
	r.User.Email = u.Email
	r.User.Password = u.Password
	if u.Bio != nil {
		r.User.Bio = *u.Bio
	}
	if u.Image != nil {
		r.User.Image = *u.Image
	}
}

func (r *userUpdateRequest) bind(c echo.Context, u *model.User) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	if err := c.Validate(r); err != nil {
		return err
	}
	u.Username = r.User.Username
	u.Email = r.User.Email
	if r.User.Password != u.Password {
		h, err := u.HashPassword(r.User.Password)
		if err != nil {
			return err
		}
		u.Password = h
	}
	u.Bio = &r.User.Bio
	u.Image = &r.User.Image
	return nil
}

type userRegisterRequest struct {
	User struct {
		Username string `json:"username" validate:"required"`
		Email    string `json:"email" validate:"required,email"`
		Password string `json:"password" validate:"required"`
	} `json:"user"`
}

func (r *userRegisterRequest) bind(c echo.Context, u *model.User) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	if err := c.Validate(r); err != nil {
		return err
	}
	u.Username = r.User.Username
	u.Email = r.User.Email
	h, err := u.HashPassword(r.User.Password)
	if err != nil {
		return err
	}
	u.Password = h
	return nil
}

type userLoginRequest struct {
	User struct {
		Email    string `json:"email" validate:"required,email"`
		Password string `json:"password" validate:"required"`
	} `json:"user"`
}

func (r *userLoginRequest) bind(c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	if err := c.Validate(r); err != nil {
		return err
	}
	return nil
}

type planCreateRequest struct {
	Plan struct {
		Title       string    `json:"title" validate:"required"`
		Description string    `json:"description" validate:"required"`
		Image       string    `json:"image" validate:"required"`
		Width       int       `json:"width" validate:"required"`
		Height      int       `json:"height" validate:"required"`
		Start_date  time.Time `json:"start_date" validate:"required"`
		End_date    time.Time `json:"end_date" validate:"required"`
		Location    string    `json:"location" validate:"required"`
		Tags        []string  `json:"tagList, omitempty"`
		SportID     uint      `json:"sport", validate:"required"`
	} `json:"plan"`
}

func (r *planCreateRequest) bind(c echo.Context, a *model.Plan) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	if err := c.Validate(r); err != nil {
		return err
	}
	a.Title = r.Plan.Title
	a.Slug = slug.Make(r.Plan.Title)
	a.Description = r.Plan.Description
	a.Image = r.Plan.Image
	a.Width = r.Plan.Width
	a.Height = r.Plan.Height
	a.Start_date = r.Plan.Start_date
	a.End_date = r.Plan.End_date
	a.Location = r.Plan.Location
	if r.Plan.Tags != nil {
		for _, t := range r.Plan.Tags {
			a.Tags = append(a.Tags, model.Tag{Tag: t})
		}
	}
	a.SportID = r.Plan.SportID
	return nil
}

type planUpdateRequest struct {
	Plan struct {
		Title       string    `json:"title"`
		Description string    `json:"description"`
		Image       string    `json:"image"`
		Width       int       `json:"width"`
		Height      int       `json:"height"`
		Start_date  time.Time `json:"start_date"`
		End_date    time.Time `json:"end_date"`
		Location    string    `json:"location"`
		Tags        []string  `json:"tagList"`
		SportID     uint      `json:"sport"`
		participant string    `json:"participant"`
	} `json:"plan"`
}

func (r *planUpdateRequest) populate(a *model.Plan) {
	r.Plan.Title = a.Title
	r.Plan.Description = a.Description
	r.Plan.Image = a.Image
	r.Plan.Width = a.Width
	r.Plan.Height = a.Height
	r.Plan.Start_date = a.Start_date
	r.Plan.End_date = a.End_date
	r.Plan.Location = a.Location
	r.Plan.SportID = a.SportID
}

func (r *planUpdateRequest) bind(c echo.Context, a *model.Plan) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	if err := c.Validate(r); err != nil {
		return err
	}
	a.Title = r.Plan.Title
	a.Slug = slug.Make(a.Title)
	a.Description = r.Plan.Description
	a.Image = r.Plan.Image
	a.Width = r.Plan.Width
	a.Height = r.Plan.Height
	a.Start_date = r.Plan.Start_date
	a.End_date = r.Plan.End_date
	a.Location = r.Plan.Location
	a.SportID = r.Plan.SportID
	return nil
}

type contactRequest struct {
	ContactData struct {
		Email   string `json:"email" validate:"required,email"`
		Subject string `json:"subject" validate:"required"`
		Body    string `json:"body" validate:"required"`
	} `json:"contactData"`
}

func (r *contactRequest) bind(c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	if err := c.Validate(r); err != nil {
		return err
	}
	return nil
}
