package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Plan struct {
	gorm.Model
	Slug         string `gorm:"unique_index;not null"`
	Title        string `gorm:"not null"`
	Description  string
	Image        string
	Width        int `gorm:"size:6"`
	Height       int `gorm:"size:6"`
	Start_date   time.Time
	End_date     time.Time
	Active       int
	Location     string
	Author       User
	AuthorID     uint
	Participants []User `gorm:"many2many:plan_participants;association_autocreate:false"`
	Sport        Sport
	SportID      uint
	Tags         []Tag `gorm:"many2many:plan_tags;association_autocreate:false"`
}

type Sport struct {
	gorm.Model
	Refer uint
	Sport string `gorm:"unique_index"`
	Plans []Plan `gorm:"foreignkey:SportID;association_foreignkey:Refer"`
}

type Tag struct {
	gorm.Model
	Tag   string `gorm:"unique_index"`
	Plans []Plan `gorm:"many2many:plan_tags;"`
}
