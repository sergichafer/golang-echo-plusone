# PlusOne

A golang based backend educational purposes project

## Getting Started

This is an that has been built based on a few requirements:

HOMEPAGE;
LOGIN;
REGISTER;
PROFILE;
PROFILE SETTINGS;
LIST;
DETAILS;
CONTACT;
TESTING;

> ### Golang/Echo codebase containing real world examples (CRUD, auth, advanced patterns, etc) using [React/Mobx]
(https://gitlab.com/sergislm4/react-mobx-plusone) frontend.

## Getting started

### Install Golang

Please check the official golang installation guide before you start. [Official Documentation](https://golang.org/doc/install)

### Environment Config
make sure your ~/.*shrc have those variable:
```
➜  echo $GOPATH
/Users/YOUR_USER/projects/
➜  echo $GOROOT
/usr/local/go/
➜  echo $PATH
...:/usr/local/go/bin:/Users/YOUR_USER/projects//bin:/usr/local/go//bin
```
For more info and detailed instructions please check this guide: [Setting GOPATH](https://github.com/golang/go/wiki/SettingGOPATH) 

### Install dep
https://golang.github.io/dep/docs/installation.html

### Clone the repository
Clone this repository to your $GOPATH:
```
➜ git clone https://gitlab.com/sergislm4/golang-echo-plusone.git  $GOPATH/src/gitlab.com/sergislm4/golang-echo-plusone
```

Switch to the repo folder
```
➜ cd $GOPATH/src/gitlab.com/sergislm4/golang-echo-plusone
```

### Install dependencies
```
➜ dep ensure -v
```

### Run
```
➜ go run main.go
```

### Build
```
➜ go build
```

### Tests
```
➜ go test ./...
```

## Authors

* **Sergi Chàfer** 

## License 

This project was based on the useful goThinkster frontends/backends repos.
* [GoThinkster Realworld App Repo](https://github.com/gothinkster/realworld) - Realworld App

All of the codebases are MIT licensed unless otherwise specified.