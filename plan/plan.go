package plan

import (
	"gitlab.com/sergislm4/golang-echo-plusone/model"
)

type Store interface {
	GetBySlug(string) (*model.Plan, error)
	GetUserPlanBySlug(userID uint, slug string) (*model.Plan, error)
	CreatePlan(*model.Plan) error
	UpdatePlan(*model.Plan, []string) error
	JoinPlan(*model.Plan, uint) error
	DeletePlan(*model.Plan) error
	List(offset, limit int) ([]model.Plan, int, error)
	ListBySport(sport string, offset, limit int) ([]model.Plan, int, error)
	ListByTag(tag string, offset, limit int) ([]model.Plan, int, error)
	ListByAuthor(username string, offset, limit int) ([]model.Plan, int, error)
	ListFeed(userID uint, offset, limit int) ([]model.Plan, int, error)
	LatestPlans() (int, error)
	EndPlans() (int, error)
	ListSports() ([]model.Sport, error)
	ListTags() ([]model.Tag, error)
}
