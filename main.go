package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/robfig/cron"
	"gitlab.com/sergislm4/golang-echo-plusone/db"
	"gitlab.com/sergislm4/golang-echo-plusone/handler"
	"gitlab.com/sergislm4/golang-echo-plusone/router"
	"gitlab.com/sergislm4/golang-echo-plusone/store"
)

func main() {
	godotenv.Load()

	r := router.New()
	v1 := r.Group("/api")

	d := db.New()
	db.AutoMigrate(d)

	us := store.NewUserStore(d)
	as := store.NewPlanStore(d)
	h := handler.NewHandler(us, as)
	c := cron.New()
	c.AddFunc("0 */15 * * * *", func() {
		var count int
		count, err := as.LatestPlans()
		if count > 0 && err != nil {
			fmt.Println("Plans started successfully")
		}
	})

	c.AddFunc("@daily", func() {
		var count int
		count, err := as.EndPlans()
		if count > 0 && err != nil {
			fmt.Println("Plans ended successfully")
		}
	})

	c.Start()
	h.Register(v1)
	r.Logger.Fatal(r.Start(":" + os.Getenv("PORT")))
}
