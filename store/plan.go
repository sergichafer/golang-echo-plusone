package store

import (
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/sergislm4/golang-echo-plusone/model"
)

type PlanStore struct {
	db *gorm.DB
}

func NewPlanStore(db *gorm.DB) *PlanStore {
	return &PlanStore{
		db: db,
	}
}

func (as *PlanStore) GetBySlug(s string) (*model.Plan, error) {
	var m model.Plan
	err := as.db.Where(&model.Plan{Slug: s}).Preload("Tags").Preload("Author").Preload("Sport").Preload("Participants").Find(&m).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &m, err
}

func (as *PlanStore) GetUserPlanBySlug(userID uint, slug string) (*model.Plan, error) {
	var m model.Plan
	err := as.db.Where(&model.Plan{Slug: slug, AuthorID: userID}).Find(&m).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &m, err
}

func (as *PlanStore) CreatePlan(a *model.Plan) error {
	tags := a.Tags
	tx := as.db.Begin()
	if err := tx.Create(&a).Error; err != nil {
		return err
	}
	for _, t := range a.Tags {
		err := tx.Where(&model.Tag{Tag: t.Tag}).First(&t).Error
		if err != nil && !gorm.IsRecordNotFoundError(err) {
			tx.Rollback()
			return err
		}
		if err := tx.Model(&a).Association("Tags").Append(t).Error; err != nil {
			tx.Rollback()
			return err
		}
	}
	if err := tx.Where(a.ID).Preload("Tags").Preload("Author").Preload("Sport").Preload("Participants").Find(&a).Error; err != nil {
		tx.Rollback()
		return err
	}
	a.Tags = tags
	return tx.Commit().Error
}

func (as *PlanStore) UpdatePlan(a *model.Plan, tagList []string) error {
	tx := as.db.Begin()
	if err := tx.Model(a).Update(a).Error; err != nil {
		return err
	}
	tags := make([]model.Tag, 0)
	for _, t := range tagList {
		tag := model.Tag{Tag: t}
		err := tx.Where(&tag).First(&tag).Error
		if err != nil && !gorm.IsRecordNotFoundError(err) {
			tx.Rollback()
			return err
		}
		tags = append(tags, tag)
	}
	if err := tx.Model(a).Association("Tags").Replace(tags).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Where(a.ID).Preload("Tags").Preload("Author").Preload("Sport").Preload("Participants").Find(a).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

func (as *PlanStore) JoinPlan(a *model.Plan, userID uint) error {
	tx := as.db.Begin()
	if err := tx.Model(a).Update(a).Error; err != nil {
		return err
	}
	var u model.User
	if err := tx.First(&u, userID).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Model(a).Association("Participants").Append(u).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Where(a.ID).Preload("Tags").Preload("Author").Preload("Sport").Preload("Participants").Find(a).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

func (as *PlanStore) DeletePlan(a *model.Plan) error {
	return as.db.Delete(a).Error
}

func (as *PlanStore) List(offset, limit int) ([]model.Plan, int, error) {
	var (
		plans []model.Plan
		count int
	)
	as.db.Model(&plans).Where("active < ?", 2).Count(&count)
	as.db.Where("active < ?", 2).Preload("Tags").Preload("Author").Preload("Sport").Preload("Participants").Offset(offset).Limit(limit).Order("created_at desc").Find(&plans)
	return plans, count, nil
}

func (as *PlanStore) ListBySport(sport string, offset, limit int) ([]model.Plan, int, error) {
	var (
		t     model.Sport
		plans []model.Plan
		count int
	)
	err := as.db.Where(&model.Sport{Sport: sport}).First(&t).Error
	if err != nil {
		return nil, 0, err
	}
	as.db.Model(&t).Where("active < ?", 2).Preload("Tags").Preload("Author").Preload("Sport").Preload("Participants").Offset(offset).Limit(limit).Order("created_at desc").Association("Plans").Find(&plans)
	count = as.db.Model(&t).Where("active < ?", 2).Association("Plans").Count()
	return plans, count, nil
}

func (as *PlanStore) ListByTag(tag string, offset, limit int) ([]model.Plan, int, error) {
	var (
		t     model.Tag
		plans []model.Plan
		count int
	)
	err := as.db.Where(&model.Tag{Tag: tag}).First(&t).Error
	if err != nil {
		return nil, 0, err
	}
	as.db.Model(&t).Where("active < ?", 2).Preload("Tags").Preload("Author").Preload("Sport").Preload("Participants").Offset(offset).Limit(limit).Order("created_at desc").Association("Plans").Find(&plans)
	count = as.db.Model(&t).Where("active < ?", 2).Association("Plans").Count()
	return plans, count, nil
}

func (as *PlanStore) ListByAuthor(username string, offset, limit int) ([]model.Plan, int, error) {
	var (
		u     model.User
		plans []model.Plan
		count int
	)
	err := as.db.Where(&model.User{Username: username}).First(&u).Error
	if err != nil {
		return nil, 0, err
	}
	as.db.Where(&model.Plan{AuthorID: u.ID}).Preload("Tags").Preload("Author").Preload("Sport").Preload("Participants").Offset(offset).Limit(limit).Order("created_at desc").Find(&plans)
	as.db.Where(&model.Plan{AuthorID: u.ID}).Model(&model.Plan{}).Count(&count)

	return plans, count, nil
}

func (as *PlanStore) ListFeed(userID uint, offset, limit int) ([]model.Plan, int, error) {
	var (
		u     model.User
		plans []model.Plan
		count int
	)
	err := as.db.First(&u, userID).Error
	if err != nil {
		return nil, 0, err
	}
	as.db.Model(&u).Where(&model.Plan{Active: 1}).Preload("Tags").Preload("Author").Preload("Sport").Preload("Participants").Offset(offset).Limit(limit).Order("start_date asc").Association("Participation").Find(&plans)
	count = as.db.Model(&u).Where(&model.Plan{Active: 1}).Association("Participation").Count()
	return plans, count, nil
}

func (as *PlanStore) ListSports() ([]model.Sport, error) {
	var sports []model.Sport
	if err := as.db.Find(&sports).Error; err != nil {
		return nil, err
	}
	return sports, nil
}

func (as *PlanStore) ListTags() ([]model.Tag, error) {
	var tags []model.Tag
	if err := as.db.Find(&tags).Error; err != nil {
		return nil, err
	}
	return tags, nil
}

func (as *PlanStore) LatestPlans() (int, error) {
	var (
		plans []model.Plan
		count int
	)
	yesterday := time.Now().Add(-24 * time.Hour)
	today := time.Now()
	as.db.Where("active=? AND start_date BETWEEN ? AND ?", 0, yesterday, today).Order("created_at desc").Model(&plans).Count(&count)
	if count > 0 {
		as.db.Model(&plans).Where("active=? AND start_date BETWEEN ? AND ?", 0, yesterday, today).Update("active", 1)
	}
	return count, nil
}

func (as *PlanStore) EndPlans() (int, error) {
	var (
		plans []model.Plan
		count int
	)
	today := time.Now()
	as.db.Where("active=? AND end_date < ?", 1, today).Order("created_at desc").Model(&plans).Count(&count)
	if count > 0 {
		as.db.Model(&plans).Where("active=? AND end_date < ?", 1, today).Update("active", 2)
	}
	return count, nil
}
