package db

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/sergislm4/golang-echo-plusone/model"
)

// Instance to open a connection with the requested database from the .env "MODE"
func New() *gorm.DB {
	if os.Getenv("MODE") == "development" {
		db, err := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=plusone password=sergislm4")
		if err != nil {
			fmt.Println("storage err: ", err)
		}
		db.DB().SetMaxIdleConns(3)
		db.LogMode(true)
		return db
	} else {
		db, err := gorm.Open("postgres", "host=ec2-54-217-208-105.eu-west-1.compute.amazonaws.com port=5432 user=eqomjmhghbijwu dbname=d7c0k9g1fd2gc4 password=85eac3a526a1aee25ddf482168925badf69eeb9e39b54e9ac2b2813004a2fe39")
		if err != nil {
			fmt.Println("storage err: ", err)
		}
		db.DB().SetMaxIdleConns(3)
		db.LogMode(true)
		return db
	}
}

// Function used to open a instance when using tests
func TestDB() *gorm.DB {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=plusone password=sergislm4")
	if err != nil {
		fmt.Println("storage err: ", err)
	}
	db.DB().SetMaxIdleConns(3)
	db.LogMode(false)
	return db
}

// Function used to clear the database before running the tests
func DropTestDB(db *gorm.DB) {
	db.Exec("TRUNCATE TABLE users;")
	db.Exec("TRUNCATE TABLE plans;")
	db.Exec("TRUNCATE TABLE sports;")
	db.Exec("TRUNCATE TABLE tags;")
	db.Exec("TRUNCATE TABLE plan_tags;")
}

// Function used to migrate the database once in case of a change
func AutoMigrate(db *gorm.DB) {
	db.AutoMigrate(
		&model.User{},
		&model.Plan{},
		&model.Sport{},
		&model.Tag{},
	)
}
